/*=============================================
Base
=============================================*/

p,
li {
    line-height: 1.7em;
}
ol,
ul {
    padding-left: 1.2em;
}

ul > li {
    list-style-type: square;
}
ol > li {
    list-style-type: decimal;
}
.fwb {
    font-weight: bold;
}
.text-red {
    color: #f04837;
}
.text-blue {
    color: #357be6;
}
.text-orange {
    color: #ff6600;
}
.btns {
    text-align: center;
}
.btn {
    display: inline-flex;
    justify-content: center;
    align-items: center;
    vertical-align: middle;
    width: 280px;
    height: 4em;
    margin: 10px;
    color: #fff;
    border-radius: 5px;
    font-size: 1.2em;
    padding: 15px 0;
    text-align: center;
    text-shadow: 0 0 5px #666;

    &--yellow {
        background: url(img/btn-yellow.png) no-repeat center;
        background-size: cover;
    }
    &--blue {
        background: url(img/btn-blue.png) no-repeat center;
        background-size: cover;
    }
    &--orange {
        background: url(img/btn-orange.png) no-repeat center;
        background-size: cover;
    }
}
.hidden-phone {
    display: none;
}

@media (min-width: 768px) {
    .hidden-phone {
        display: block;
    }
}

.visible-phone {
    display: block;
}
.fixed-rocket {
    width: 70px;
    position: fixed;
    right: 5px;
    z-index: 10;
    bottom: 100px;
}
@media (min-width: 768px) {
    .visible-phone {
        display: none;
    }
}

@media only screen and (max-width: 960px) {
    .fixed-rocket {
        width: 60px;
        right: 2px;
        bottom: 120px;
    }
}
/*=============================================
Main
=============================================*/
.main {
    padding-top: 80px;
    overflow: hidden;
}
/*=============================================
KV
=============================================*/
.KV {
    position: relative;
    height: 640px;
    background: url(img/kv-bg2.jpg) no-repeat center;
    background-size: cover;
    &__wrap {
        position: relative;
        width: 1040px;
        margin: 0 auto;
        text-align: center;
        height: 100%;
        transform: scale(0.67);
    }
}
.KVContent {
    position: relative;
    height: 100%;
}
.kv-arrow {
    width: 100%;
    max-width: 40px;
    position: absolute;
    left: 0;
    right: 0;
    bottom: -20px;
    margin: 0 auto;
    z-index: 5;
    animation: kvArrow 2s infinite;
}
.kv {
    position: absolute;
    &-txt {
        // top: 55px;
        top: -110px;
        left: 55px;
    }
    &-bar {
        // bottom: 80px;
        bottom: -20px;

        left: 255px;
        z-index: 10;
    }
    &-calendar {
        // bottom: 155px;
        bottom: 55px;

        left: 135px;
    }
    &-coins {
        // bottom: 240px;
        bottom: 130px;
        right: 316px;
        z-index: 15;
    }
    &-diamond {
        // bottom: 475px;
        bottom: 290px;
        right: 480px;
        z-index: 20;
    }
    &-rocket {
        right: 100px;
        // bottom: 190px;
        bottom: 80px;
        z-index: 10;
    }
    &-phone {
        // bottom: 10px;
        bottom: -110px;
        right: 245px;
    }
}
@keyframes kvArrow {
    0% {
        -webkit-transform: translate(0, 0);
        transform: translate(0, 0);
    }

    25% {
        -webkit-transform: translate(0, 25%);
        transform: translate(0, 25%);
    }

    50% {
        -webkit-transform: translate(0, 0);
        transform: translate(0, 0);
    }

    100% {
        -webkit-transform: translate(0, 0);
        transform: translate(0, 0);
    }
}

@media only screen and (max-width: 960px) {
    .main {
        padding-top: 58px;
    }
    .KV {
        background: url(img/kv-bg-m.jpg) no-repeat center;
        background-size: cover;
        width: 100%;
        // overflow: hidden;
        position: relative;
        height: 0;
        padding-bottom: 126%;
        &__wrap {
            width: 100%;
        }
    }
    .KVContent {
        display: none;
    }
    .kv-arrow {
        bottom: -20px;
    }
}

/*=============================================
Section
=============================================*/
.section {
    position: relative;
    &__wrapper {
        max-width: 1300px;
        margin: 0 auto;
        padding: 50px 15px;
    }
    &__title {
        position: relative;
        font-size: 2em;
        color: #357be6;
        text-align: center;
        margin-bottom: 80px;
        &::after {
            content: '';
            display: block;
            position: absolute;
            top: 40px;
            left: 50%;
            transform: translateX(-50%);
            height: 35px;
            width: 340px;
            background: url(img/dec-diamond-line.png) no-repeat center;
            background-size: contain;
        }
    }
}

/*=============================================
Section1
=============================================*/
.section-01 {
    background: url(img/bg-section01.jpg) repeat-x top;
    background-size: auto 100%;
    padding-bottom: 30px;
}
.mission {
    display: flex;
    justify-content: center;
    align-items: center;
    flex-wrap: nowrap;
    flex-direction: row;
    &__header {
        position: relative;
        margin-right: 50px;
        img {
            width: 200px;
        }
        &::after {
            position: absolute;
            top: 50%;
            right: -60px;
            transform: translateX(-50%);
            content: '';
            display: block;
            width: 30px;
            height: 1px;
        }
    }
    &__body {
        position: relative;
        background: linear-gradient(#fff, #ececec);
        width: 650px;
        margin: 10px;
        padding: 10px 50px;
        border-radius: 15px;
        color: #666;
        min-height: 200px;
        &::after {
            position: absolute;
            content: '';
            display: block;
            width: 30px;
            height: 100%;
            top: 0;
            right: 10px;
        }
        .title {
            color: #2f6ed2;
            font-size: 1.4em;
            text-align: center;
            &::before {
                background: url(img/title-coin.png) no-repeat center;
                background-size: contain;
                width: 30px;
                height: 30px;
                display: inline-block;
                content: '';
                margin-right: 10px;
                margin-bottom: 5px;
                vertical-align: middle;
            }
        }

        ul {
            .sub {
                position: relative;
                font-size: 0.9em;
                list-style: none;
                &::before {
                    position: absolute;
                    left: -1.6em;
                    top: 2px;
                    content: '';
                    display: inline-block;
                    height: 20px;
                    width: 20px;
                    background-image: url(img/list-style-star.png);
                    background-size: contain;
                }
            }
        }
    }
}
.mission-01 {
    .mission__body {
        border: 2px solid #50fffe;
        &::after {
            background: url(img/bg-lock-box1.png) no-repeat center;
        }
    }
    .mission__header {
        &::after {
            border-bottom: 2px dashed #50fffe;
        }
    }
}
.mission-02 {
    .mission__body {
        border: 2px solid #fcb13f;
        &::after {
            background: url(img/bg-lock-box2.png) no-repeat center;
        }
    }
    .mission__header {
        &::after {
            border-bottom: 2px dashed #fcb13f;
        }
    }
}
.mission-03 {
    .mission__body {
        border: 2px solid #ee9970;
        &::after {
            background: url(img/bg-lock-box3.png) no-repeat center;
        }
    }
    .mission__header {
        &::after {
            border-bottom: 2px dashed #ee9970;
        }
    }
}
@media only screen and (max-width: 768px) {
    .mission {
        flex-direction: column;
        &__body {
            width: 100%;
        }
        &__header {
            margin-right: 0;
            margin-bottom: 30px;
            &::after {
                top: 110%;
                left: 50%;
                transform: translate(-50%, -50%) rotate(90deg);

                width: 30px;
                height: 1px;
            }
        }
    }
}
/*=============================================
Section2
=============================================*/
.section-02 {
    background: url(img/bg-section02.jpg) no-repeat center;
    background-size: cover;
}
.infoPanel {
    position: relative;
    padding: 50px 25px;
    color: #666;
    margin: 60px 0;
    font-size: 1em;
    max-width: 1000px;
    margin: 0 auto;
    // pointer-events: none;
    .title {
        position: relative;
        color: #3366cc;
        font-size: 1.2em;
        margin-top: 10px;
        background-color: rgba(#3366cc, 0.1);
        border-radius: 5px;
        padding: 5px 30px 5px 10px;
        cursor: pointer;
        &::before {
            background: url(img/dec-list-title.png) no-repeat center;
            background-size: contain;
            width: 40px;
            height: 40px;
            display: inline-block;
            content: '';
            vertical-align: middle;
        }
        &::after {
            position: absolute;
            font-weight: bold;
            top: 50%;
            right: 5px;
            font-size: 1.5em;
            transform: translateY(-50%);
            content: '－';
        }
        &.open {
            &::after {
                content: '＋';
            }
        }
    }

    li {
        line-height: 2em;
    }
    .content {
        position: relative;
        z-index: 2;
    }
    &::before {
        z-index: 2;
        position: absolute;
        right: -100px;
        top: -450px;
        background: url(img/dec-blueRocket.png) no-repeat center;
        background-size: contain;
        width: 377px;
        height: 517px;
        display: block;
        content: '';
        pointer-events: none;
    }
    &::after {
        position: absolute;
        z-index: 1;
        content: '';
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        background: linear-gradient(#e1f4ff, #edfbfe);
        border: 1px solid #53d4ff;
        box-shadow: 0px 0px 5px #53d4ff;
        border-radius: 10px;
        transform: skewY(2deg);
    }
}
@media only screen and (max-width: 960px) {
    .infoPanel {
        &::before {
            right: -50px;
            top: -250px;
            width: 200px;
            height: 300px;
        }
    }
}

/*=============================================
Section3
=============================================*/

.section-03 {
    background: linear-gradient(#b9edff, #ecfdff);
    text-align: center;
}
.discount-title {
    color: #ff761c;
    font-size: 1.5em;
    max-width: 300px;
    margin: 0 auto;
}
.goldBoxRow {
    margin: 50px 0;
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    flex-wrap: wrap;
}

.goldBox {
    p {
        padding: 0;
    }
    position: relative;
    width: 290px;
    height: 350px;
    background: url(img/bg-goldbox.png) no-repeat center;
    background-size: contain;
    text-align: center;
    padding: 20px 20px 80px 20px;
    margin: 20px;

    display: flex;
    flex-direction: column;
    justify-content: space-between;

    .title {
        font-size: 1.5em;
        font-weight: bold;
        color: #e52e3c;
    }
    .info {
        font-size: 1.2em;
        color: #333;
    }
    &__btn {
        color: #fff;
        display: block;
        width: 200px;
        height: 70px;
        margin: 0 auto;
        background: url(img/bg-goldbox-btn.png) no-repeat center;
        background-size: contain;
        font-size: 1.6em;
        padding: 0.5em 0;
    }
}
@media only screen and (max-width: 768px) {
    .goldBoxRow {
        justify-content: space-around;
        margin: 25px 0;
    }
    .goldBox {
        width: 165px;
        height: 195px;
        font-size: 0.7em;
        padding: 10px 10px 40px 10px;
        margin: 2px;
        &__btn {
            width: 90px;
            height: 34px;
            font-size: 1.2em;
        }
    }
}
/*=============================================
appDownload
=============================================*/

.appDownload-container {
    width: 100%;
    text-align: center;
    background-color: #e1f3fe;
    padding: 1em;
}

.appDownload h5 {
    font-size: 0.8em;
    line-height: 1.3em;
    color: #000;
    margin-bottom: 13px;
}

@media (min-width: 768px) {
    .appDownload h5 {
        font-size: 0.95em;
        font-weight: bold;
    }
}

.appDownload h4 {
    font-size: 1.125em;
    line-height: 2em;
    color: #000;
    margin-bottom: 0.6em;
}

.appDownload-appQRcode {
    display: none;
}

@media (min-width: 992px) {
    .appDownload-appQRcode {
        display: inline-block;
        vertical-align: middle;
        padding-right: 1.2em;
    }
}

.appDownload-appQRcode img {
    max-width: 100px;
}

.appDownload-appLink {
    display: inline-block;
    vertical-align: middle;
}

.appDownload-appLink a {
    width: 45%;
    max-width: 170px;
    display: inline-block;
    vertical-align: middle;
    margin: 0 0.2em;
    -webkit-transition: 0.15s 0.02s ease-in all;
    -o-transition: 0.15s 0.02s ease-in all;
    transition: 0.15s 0.02s ease-in all;
}

@media (min-width: 768px) {
    .appDownload-appLink a {
        width: 140px;
    }
}

@media (min-width: 1200px) {
    .appDownload-appLink a {
        width: 170px;
    }

    .appDownload-appLink a:hover {
        -webkit-transform: translate(0, -10%);
        -ms-transform: translate(0, -10%);
        transform: translate(0, -10%);
    }
}

/*=============================================
RemarksSection
=============================================*/

.remarksSection {
    width: 100%;
    margin-bottom: 50px;
}

.remarksSection-border {
    border: 1px solid #fff;
    border-radius: 9px;
    background-color: #28a4fb;
}

.remarksSection-btn {
    padding: 15px 20px;
    position: relative;
}

@media (min-width: 768px) {
    .remarksSection-btn {
        padding: 15px 30px;
    }
}

.remarksSection-title {
    text-align: left;
    font-size: 1.25em;
    font-weight: 700;
    color: #fff;
}

.remarksSection-icon {
    width: 20px;
    height: 20px;
    background-image: url(img/accordion_minus.png);
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
    position: absolute;
    right: 20px;
    top: 50%;
    transform: translate(0, -50%);
}

@media (min-width: 768px) {
    .remarksSection-icon {
        right: 40px;
    }
    .remarksSection-content {
        padding: 25px 40px;
    }
}

.remarksSection-icon.hide {
    background-image: url(img/accordion_plus.png);
}

.remarksSection-hr {
    width: 96%;
    height: 1px;
    background: #fff;
    margin: 0 auto;
}

.remarksSection-hr.hide {
    background: transparent;
}

.remarksSection-content {
    padding: 15px 20px;
    font-size: 0.95em;
    line-height: 1.7em;
    font-weight: bold;
    text-align: justify;
    color: #fff;
    & > ol {
        list-style-type: decimal;
        padding-left: 1.6em;
        & > ol {
            padding-left: 0;
        }
        & ol > li {
            position: relative;
            list-style-type: none;
            list-style-position: inside;
            counter-increment: cnt;
        }
        & ol > li:before {
            display: marker;
            content: '(' counter(cnt) ') ';
            position: absolute;
            left: -1.4em;
        }
    }
}

.remarksSection-contentBlock {
    margin-bottom: 30px;
}

.remarksSection-contentBlock:last-child {
    margin-bottom: 0;
}

* {
    animation-duration: calc(1s * 0.75);
    animation-duration: calc(var(--animate-duration) * 0.75);
}

.transferContent {
    position: relative;
    padding: 30px 20px;
    font-size: 1em;
    max-width: 1000px;
    margin: 0 auto;
    pointer-events: none;

    &::after {
        position: absolute;
        pointer-events: none;
        content: '';
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        background: linear-gradient(#7bcafd, #d5f2fe);
        border: 1px solid #53d4ff;
        box-shadow: 0px 0px 5px #53d4ff;
        border-radius: 10px;
        transform: skewY(2deg);
        z-index: -1;
    }
}

.transferTitle {
    border: 3px solid #3333ff;
    color: #3333ff;
    font-size: 1.6em;
    padding: 10px 20px;
    display: inline-block;
    font-weight: bold;
    span {
        font-size: 1.5em;
        margin: 0 5px;
    }
}
.transferDesc {
    text-align: center;
    &__block {
        display: inline-block;
        width: 40%;
        margin: 10px;
        vertical-align: top;
        text-align: center;
        .title {
            font-size: 1.2em;
            color: #3333ff;
            font-weight: bold;
            margin-bottom: 10px;
            &::before {
                display: inline-block;
                vertical-align: sub;
                content: '';
                width: 24px;
                height: 24px;
                background: url(img/icon-gift.png) no-repeat center;
                background-size: cover;
            }
            span {
                margin: 0 5px;
                font-size: 1.5em;
            }
        }
        .img {
            margin: 20px auto;
            max-width: 200px;
        }
        .info {
            text-align: left;
            font-size: 1em;
            color: #666;
            line-height: normal;
            span {
                color: #ff6600;
                font-weight: bold;
                margin: 0;
                font-size: 1.5em;
            }
        }
    }
}
@media only screen and (max-width: 768px) {
    .transferContent {
        padding: 20px 10px;
    }
    .transferTitle {
        font-size: 1.2em;
        padding: 5px 10px;
    }
    .transferDesc {
        &__block {
            margin: 5px;
            width: 45%;
            .img {
                max-width: 250px;
            }
            .title {
                font-size: 1em;
            }
            .info {
                font-size: 0.8em;
                span {
                    margin: 0 2px;
                }
            }
        }
    }
}
//modal
.modal-overlay {
    position: fixed;
    top: 0;
    left: 0;
    z-index: 100;
    width: 100%;
    height: 100%;
    visibility: hidden;
    opacity: 0;
    background: rgba(30, 30, 30, 0.8);
    transition: 0.3s;
}
.modal-container {
    position: fixed;
    max-width: 780px;
    top: 50%;
    left: 50%;
    z-index: 200;
    text-align: center;
    width: 90%;
    visibility: hidden;
    transform: translate(-50%, -50%);
}
.modal-content {
    position: relative;
    opacity: 0;
    transition: 0.3s;
    // background: #fff;
}
.modal-close {
    position: absolute;
    top: 30px;
    right: 3%;
    width: 50px;
    height: 50px;
    cursor: pointer;
    img {
        cursor: pointer;
    }
}

.modal-content__img {
    width: 100%;
    position: absolute;
    left: 0;
    top: 0;
    z-index: -1;
}

.modal--show {
    visibility: visible;
}
.modal--show ~ .modal-overlay {
    visibility: visible;
    opacity: 1;
}
.modal--show .modal-content {
    transform: scale(1);
    opacity: 1;
}

@media only screen and (max-width: 768px) {
    .modal-close {
        top: 20px;
        right: 3%;
        width: 35px;
        height: 35px;
    }
}
