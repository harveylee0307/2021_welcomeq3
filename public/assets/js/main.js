!(function (a) {
    var b = /iPhone/i,
        c = /iPod/i,
        d = /iPad/i,
        e = /(?=.*\bAndroid\b)(?=.*\bMobile\b)/i,
        f = /Android/i,
        g = /(?=.*\bAndroid\b)(?=.*\bSD4930UR\b)/i,
        h =
            /(?=.*\bAndroid\b)(?=.*\b(?:KFOT|KFTT|KFJWI|KFJWA|KFSOWI|KFTHWI|KFTHWA|KFAPWI|KFAPWA|KFARWI|KFASWI|KFSAWI|KFSAWA)\b)/i,
        i = /Windows Phone/i,
        j = /(?=.*\bWindows\b)(?=.*\bARM\b)/i,
        k = /BlackBerry/i,
        l = /BB10/i,
        m = /Opera Mini/i,
        n = /(CriOS|Chrome)(?=.*\bMobile\b)/i,
        o = /(?=.*\bFirefox\b)(?=.*\bMobile\b)/i,
        p = new RegExp('(?:Nexus 7|BNTV250|Kindle Fire|Silk|GT-P1000)', 'i'),
        q = function (a, b) {
            return a.test(b)
        },
        r = function (a) {
            var r = a || navigator.userAgent,
                s = r.split('[FBAN')
            if (
                ('undefined' != typeof s[1] && (r = s[0]),
                (s = r.split('Twitter')),
                'undefined' != typeof s[1] && (r = s[0]),
                (this.apple = {
                    phone: q(b, r),
                    ipod: q(c, r),
                    tablet: !q(b, r) && q(d, r),
                    device: q(b, r) || q(c, r) || q(d, r),
                }),
                (this.amazon = {
                    phone: q(g, r),
                    tablet: !q(g, r) && q(h, r),
                    device: q(g, r) || q(h, r),
                }),
                (this.android = {
                    phone: q(g, r) || q(e, r),
                    tablet: !q(g, r) && !q(e, r) && (q(h, r) || q(f, r)),
                    device: q(g, r) || q(h, r) || q(e, r) || q(f, r),
                }),
                (this.windows = { phone: q(i, r), tablet: q(j, r), device: q(i, r) || q(j, r) }),
                (this.other = {
                    blackberry: q(k, r),
                    blackberry10: q(l, r),
                    opera: q(m, r),
                    firefox: q(o, r),
                    chrome: q(n, r),
                    device: q(k, r) || q(l, r) || q(m, r) || q(o, r) || q(n, r),
                }),
                (this.seven_inch = q(p, r)),
                (this.any =
                    this.apple.device ||
                    this.android.device ||
                    this.windows.device ||
                    this.other.device ||
                    this.seven_inch),
                (this.phone = this.apple.phone || this.android.phone || this.windows.phone),
                (this.tablet = this.apple.tablet || this.android.tablet || this.windows.tablet),
                'undefined' == typeof window)
            )
                return this
        },
        s = function () {
            var a = new r()
            return (a.Class = r), a
        }
    'undefined' != typeof module && module.exports && 'undefined' == typeof window
        ? (module.exports = r)
        : 'undefined' != typeof module && module.exports && 'undefined' != typeof window
        ? (module.exports = s())
        : 'function' == typeof define && define.amd
        ? define('isMobile', [], (a.isMobile = s()))
        : (a.isMobile = s())
})(this)

function showHideArea() {
    var e = $('#remarksSection-content')
    $('.remarksSection-hr').addClass('hide'),
        $('.remarksSection-icon').addClass('hide'),
        e.slideUp()
}
function getUrlParam(name) {
    var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)')
    var r = window.location.search.substr(1).match(reg)
    if (r != null) return unescape(r[2])
    return null
}
function scrollTo(element) {
    var anchor = $('.' + element).offset().top
    $('html,body').animate({ scrollTop: anchor - 80 }, 1000)
}
$(function () {
    new WOW({ offset: 150 }).init()
    showHideArea()
    var param = getUrlParam('scrollTo')

    if (param) {
        setTimeout(function () {
            scrollTo(param)
        }, 100)
    }
    //按鈕

    $('a[data-scrollTo]').click(function (e) {
        var target = e.target.dataset.scrollto
        scrollTo(target)
    })
    $('.remarksSection-btn').on('click', function (e) {
        e.preventDefault()
        $('#remarksSection-content').slideToggle(500)
        $('.remarksSection-hr').toggleClass('hide')
        $('.remarksSection-icon').toggleClass('hide')
    })
    //modal
    var $modalOverlay = $('.modal-overlay'),
        $modalContainer = $('.modal-container'),
        $modalTrigger = $('.modal-trigger'),
        $modalClose = $('.modal-close')

    $modalTrigger.on('click', function () {
        $modalContainer.toggleClass('modal--show')
    })

    $modalOverlay.on('click', function () {
        $modalContainer.toggleClass('modal--show')
    })

    $modalClose.on('click', function () {
        $modalContainer.removeClass('modal--show')
    })
    //任務收合
    $('.infoPanel .title').on('click', function () {
        $(this).toggleClass('open')
        $(this).next('ol').slideToggle()
    })
    //動態
    var t = gsap.timeline({
        delay: 0.5,
    })
    t.from('.kv-phone', { duration: 0.5, y: 100, opacity: 0 })
        .from('.kv-dec', { duration: 1, y: 20, opacity: 0, stagger: 0.1, ease: 'elastic' })
        .from('.kv-rocket', {
            duration: 0.5,
            x: -150,
            y: 150,
            scale: 0.1,
            opacity: 0,
        })
        .from('.kv-diamond', { duration: 1, y: 20, opacity: 0, ease: 'elastic' })
    gsap.fromTo('.kv-phone img', 1.5, { y: '5' }, { y: '-5', ease: 'none', repeat: -1, yoyo: !0 })
    gsap.fromTo('.kv-calendar img', 1, { y: '-2' }, { y: '2', ease: 'none', repeat: -1, yoyo: !0 })
    gsap.fromTo( '.kv-diamond img,.fixed-rocket', 1, { y: '7' },{ y: '-7', ease: 'none', repeat: -1, yoyo: !0 })
    gsap.fromTo('.kv-rocket img', 2, { x: '5' }, { x: '-5', ease: 'none', repeat: -1, yoyo: !0 })
    gsap.fromTo('.kv-coins img', 1.2, { y: '-4' }, { y: '4', ease: 'none', repeat: -1, yoyo: !0 })
    gsap.fromTo('.kv-bar img', 1.2, { y: '4' }, { y: '-4', ease: 'none', repeat: -1, yoyo: !0 })
})
